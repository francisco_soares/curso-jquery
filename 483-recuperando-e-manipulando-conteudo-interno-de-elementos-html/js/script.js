// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {
    // O [.html()] recupera os conteúdos internos de uma tag, nesse caso a [#div1]
    console.log('Conteúdo DIV 1: ', $('#div1').html())
    console.log('Conteúdo DIV 2: ', $('#div2').html())

    // Também com o [.html()], eu posso alterar o valor de uma tag, nesse caso estou alterando no [#div1]
    // Caso queira colocar formatar, isso será tranquilo.
    console.log($('#div1').html('<strong style="color: red;">Novo valor inserido na DIV</strong>'))
})