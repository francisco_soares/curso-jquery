// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {

    // ON/OFF - Registra/Remove eventos para os elemento HTML mesmo que eles sejam criados posteriormente a chamada da função.

    // ON
    // O [e] está informando que será pego as informações do próprio [INPUT]
    $('body').on('focus', 'input', e => {
        $(e.target).removeClass('desfocado')
        $(e.target).addClass('focado')
    })

    // O [e] está informando que será pego as informações do próprio [INPUT]
    $('body').on('blur', 'input', e => {
        $(e.target).removeClass('focado')
        $(e.target).addClass('desfocado')
    })

    $('body').append('<br><br> <input type="text" class="desfocado">')

    // EXPLICAÇÃO:
    // No [append] estou criando um elemento input de do form programático.
    // Para que esse novo elemento pegue as informações do FOCUS e BLUR informado acima, precisa ser alterada as funções.

    // Dessa forma na função $('body').on('focus', 'input', e => { }) é passado três parametros.
    // 1º - BODY: Onde está sendo informado que todo o efeito tem que ser nos filhos do Body.
    // 2º - FOCUS: O tipo de efeito que será implementado
    // 3º - INPUT: A tag ou CAMPO que será feito o efeito FOCUS.

    // Dessa forma, com o .on(), podemos inserir novos elementos com jQuery e passar o .on() para pegar os efeitos nessas novas tags criadas.

    // OFF
    $('body').off('blur', 'input')
})