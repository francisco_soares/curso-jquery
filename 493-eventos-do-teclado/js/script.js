// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {
    // keydown() - Executa quando tem a pressão em uma tecla.
    // AVISO: No código abaixo tem um bug e vou utilizado para corrigir o método KEYUP, veja o código abaixo.
    // $('#teclado').keydown((e) => {
    //     // $('#resultadoTecla').html('Tecla pressionada')
    //     if(e.keyCode < 97 || e.keyCode > 100){
    //         let txt = $(e.target).val()
    //         txt = txt.slice(0, -1)

    //         $(e.target).val(txt)
    //     }
    // })

    // keyup() - Executa quando tira a pressão da tecla.
    // O (e) eu pego o evento
   /* $('#teclado').keyup((e) => {

        // A idéia usada aqui serve para um CHAT
        if (e.keyCode == 13) {
            // $(e.target).val() - Trás o valor do campo [INPUT]
            let txt = $(e.target).val()
            $('#resultadoTecla').html(txt)

            // Aqui limpo o campo vázio
            $(e.target).val('')
        }
    })*/

    $('#teclado').keyup((e) => {
        // No IF abaixo está fazendo uma condição, se os campos digitas o [keyCode] for meno que 97 ou maior que 100 entra na condição.
        if(e.keyCode < 97 || e.keyCode > 100){

            // Pega o valor passado
            let txt = $(e.target).val()

            // O método slice() percorre a string ou o array para selecionar a faixa de valores determinada por parâmetro.
            // Remove o último caracter inserido.
            txt = txt.slice(0, -1)

            // E atribui no input
            $(e.target).val(txt)
        }
    })

})