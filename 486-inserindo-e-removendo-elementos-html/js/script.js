// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {
    // [before] - Inseri o conteúdo antes de uma tag, exemplo: se você tiver uma tag [p] aberta, o conteúdo será inserido antes da tag [p]
    $('#lista2').before('<h4>Inserido antes da UL</h4>')

    // [after] - Inseri o conteúdo depois de uma tag, exemplo: se você tiver uma tag [p] aberta, o conteúdo será inserido depois da tag [p]
    $('#lista2').after('<p>Inserido após a DIV</p>')
    $('#hr').after('<h3>Inserido após o HR</h3>')

    // [prepend] - Insere o conteúdo/elemento no início de uma tag. Exemplo: se você abrir um tag [ul] e quer inserir um tag [li] como primeira, você usa o [prepend]
    $('#lista1').prepend('<li>Item 5</li>')
    $('#lista2').prepend('<div>Item 5</div>')

    // [append] - Insere o conteúdo/elemento no fim de uma tag. Exemplo: se você abrir um tag [ul] e quer inserir um tag [li] como última, você usa o [prepend]
    $('#lista1').append('<li>Item 4</li>')
    $('#lista2').append('<div>Item 4</div>')    

    // [html] - Usar o [.html()] fará a substituição de todo o conteúdo de uma tag. 
    $('#filmes').html("<strong>Animes</strong><br><li>One Punch Man</li><li>Naturo</li>")

    // [remove] - Remove um elemento.
    $('#hr').remove()

    // ATENÇÃO: Todos os testes feito foram com [ID], porém, podem ser feito com: CLASS, TAG HTML e etc.
     
})