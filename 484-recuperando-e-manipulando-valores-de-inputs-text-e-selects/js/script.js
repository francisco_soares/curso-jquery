// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {
    // O [val()] retorna o valor do campo [input]
    // Para esse exemplo, eu defini hardcode o [value] do campo nome.
    // Caso queira alterar o valor dentro do value do formulário é só dentro do [val()] inserir o valor.Veja o exemplo: nome = $('#nome').val('Saitama')
    let nome = $('#nome').val()
    let origem = $('#origem').val()

    console.log(nome)
    console.log(origem)
})