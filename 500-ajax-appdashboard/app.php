<?php

// Classe Dashboard
class Dashboard
{
    // Defino os atributos
    public $data_inicio;
    public $data_fim;
    public $numeroVendas;
    public $totalVendas;
    public $clientesAtivos;
    public $clientesInativos;
    public $totalDespesa;
    public $totalReclamacao;
    public $totalElogios;
    public $totalSugestao;

    /**
     * Método mágico __get()
     *
     * @param string $atributo
     * @return string
     */
    public function __get($atributo)
    {
        return $this->$atributo;
    }

    /**
     * Método mágico __set()
     *
     * @param string $atributo
     * @param string $valor
     */
    public function __set($atributo, $valor)
    {
        $this->$atributo = $valor;
        return $this;
    }
}

// Classe de Conexão com BD
class Conexao
{
    private $host = "localhost";
    private $dbname = "dashboard";
    private $user = "root";
    private $pass = "";

    public function conectar()
    {
        try {
            // Crio a conexão via PDO
            $conexao = new PDO("mysql:host={$this->host};dbname={$this->dbname}", "$this->user", "$this->pass");

            // Informo que será utilizado o UTF8
            $conexao->exec('set charset utf8');

            return $conexao;
        } catch (PDOException $e) {
            echo "<p>{$e->getMessage()}</p>";
        }
    }
}

// Classe Model
class Bd
{
    // Atributos da classe
    private $conexao;
    private $dashboard;

    /**
     * Método construtor que recebe 2 parametros sendo instancia/obj de uma classe
     * 
     * A chamada dos objetos [Conexao] e [Dashboard] no parametro do método já faz inclusão das duas classes [Conexao] e [Dashboard]
     *
     * @param OBJ Conexao $conexao
     * @param OBJ Dashboard $dashboard
     */
    public function __construct(Conexao $conexao, Dashboard $dashboard)
    {
        // Recebe o método [conectar()]
        $this->conexao = $conexao->conectar();
        // Recebe os métodos mágico da classe [Dashboard]
        $this->dashboard = $dashboard;
    }

    /**
     * Método que retona o resultado da Query
     *
     * @return obj
     */
    public function getNumeroVendas()
    {
        // Crio a query
        $query = "
        SELECT 
            count(*) AS numero_vendas
        FROM
            tb_vendas
        WHERE
            data_venda BETWEEN :data_inicio AND :data_fim
        ";

        // Preparo a query
        $stmt = $this->conexao->prepare($query);
        // Faço os Binds
        $stmt->bindValue(':data_inicio', $this->dashboard->__get('data_inicio'));
        $stmt->bindValue(':data_fim', $this->dashboard->__get('data_fim'));
        // Executo
        $stmt->execute();

        // Retorno em formato de OBJ assim acessando o numero_vendas
        return $stmt->fetch(PDO::FETCH_OBJ)->numero_vendas;
    }

    /**
     * Método que retona o total de vendas
     *
     * @return obj total_vendas
     */
    public function getTotalVendas()
    {
        // Crio a query
        $query = "
        SELECT 
            SUM(total) AS total_vendas
        FROM
            tb_vendas
        WHERE
            data_venda BETWEEN :data_inicio AND :data_fim
        ";

        // Preparo a query
        $stmt = $this->conexao->prepare($query);
        // Faço os Binds
        $stmt->bindValue(':data_inicio', $this->dashboard->__get('data_inicio'));
        $stmt->bindValue(':data_fim', $this->dashboard->__get('data_fim'));
        // Executo
        $stmt->execute();

        // Retorno em formato de OBJ
        return $stmt->fetch(PDO::FETCH_OBJ)->total_vendas;
    }


    public function getClientesAtivos()
    {
        $query = "
        SELECT 
            SUM(cliente_ativo) AS clientesAtivos
        FROM
            tb_clientes
        WHERE 
            cliente_ativo = 1
        ";

        // Preparo a query
        $stmt = $this->conexao->prepare($query);
        // Executo
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_OBJ)->clientesAtivos;
    }

    public function getClientesInativos()
    {
        $query = "
            SELECT 
                COUNT(cliente_ativo) AS clientesInativos  
            FROM 
                tb_clientes
            WHERE
                cliente_ativo = 0
        ";

        $stmt = $this->conexao->prepare($query);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_OBJ)->clientesInativos;
    }

    public function getTotalReclamacao()
    {
        $query = "
            SELECT
                COUNT(tipo_contato) AS totalReclamacao
            FROM 
                tb_contatos
            WHERE
                tipo_contato = 1
        ";

        $stmt = $this->conexao->prepare($query);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_OBJ)->totalReclamacao;
    }

    public function getTotalElogios()
    {
        $query = "
            SELECT 
                COUNT(tipo_contato) AS totalElogios
            FROM
                tb_contatos
            WHERE
                tipo_contato = 2
        ";

        $stmt = $this->conexao->prepare($query);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_OBJ)->totalElogios;
    }

    public function getTotalSugestao()
    {
        $query = "
            SELECT 
                COUNT(tipo_contato) AS totalSugestao
            FROM
                tb_contatos
            WHERE 
                tipo_contato = 3
        ";

        $stmt = $this->conexao->prepare($query);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_OBJ)->totalSugestao;
    }

    public function getTotalDespesa()
    {
        $query = "
            SELECT 
                SUM(total) AS totalDespesa
            FROM
                tb_despesas
        ";

        $stmt = $this->conexao->prepare($query);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_OBJ)->totalDespesa;
    }
}

// Criando as instancias

$conexao = new Conexao();
$dashboard = new Dashboard();

$competencia = explode('-', $_GET['competencia']);
$ano = $competencia[0];
$mes = $competencia[1];

// Saber quantos dias tem no mês usamos a função [cal_days_in_month(<calendario>, <mes>, <ano>)]
$dias_do_mes = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);

// Como o mês sempre começa no dia 1, eu seto isso em hardcode
$dashboard->__set('data_inicio', $ano . '-' . $mes . '-01');
// Seto o último dia do mês
$dashboard->__set('data_fim', $ano . '-' . $mes . '-' . $dias_do_mes);

$bd = new Bd($conexao, $dashboard);

$dashboard->__set('numeroVendas', $bd->getNumeroVendas());
$dashboard->__set('totalVendas', $bd->getTotalVendas());
$dashboard->__set('clientesAtivos', $bd->getClientesAtivos());
$dashboard->__set('clientesInativos', $bd->getClientesInativos());
$dashboard->__set('totalDespesa', $bd->getTotalDespesa());
$dashboard->__set('totalReclamacao', $bd->getTotalReclamacao());
$dashboard->__set('totalElogios', $bd->getTotalElogios());
$dashboard->__set('totalSugestao', $bd->getTotalSugestao());

// O [json_encode] vai transcrever o objeto para um JSON e retornar para a aplicação
echo json_encode($dashboard);
