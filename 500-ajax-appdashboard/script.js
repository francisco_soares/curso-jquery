$(document).ready(() => {
    // .load() - carregue os dados do servidor e coloque o HTML retornado nos elementos correspondentes. O [load()] faz por padrão uma requisição [GET]

    // .get() - Faz uma ação quando é requisitado na aplicação
    // Sintax: $.get('<url>', <uma-acao>).
    // Na <uma-acao>, o mesmo retorna uma resposta, que pode ser qualquer nomenclatura, como: valor, retorno, data e etc

    // .get() - Faz uma ação quando é requisitado na aplicação

    $('#documentacao').on('click', function () {
        // Usando o LOAD
        // $('#pagina').load('documentacao.html')

        // Usando o GET
        /* $.get('documentacao.html', data => {
             $('#pagina').html(data)
         })*/

        // Usando o POST
        $.post('documentacao.html', data => {
            $('#pagina').html(data)
        })
    })

    $('#suporte').on('click', () => {
        // Usando o LOAD
        // $('#pagina').load('suporte.html')

        // Usando o GET
        /*$.get('suporte.html', data => {
            $('#pagina').html(data)
        })*/

        // Usando o POST
        $.post("suporte.html", data => {
            $('#pagina').html(data)
        })
    })

    // AJAX - Implementar para fazer requisições Request
    // Aqui eu posso usar ARROW FUNCTION
    //$("#competencia").on("change", e => {})
    $("#competencia").on('change', function (e) {

        // Recupera o valor/value do campo Option do formulário
        let competencia = $(e.target).val()

        // Usando o método AJAX para enviar um solicitação Request
        $.ajax({
            type: 'GET', // Tipo de requisição GET ou POST
            url: 'app.php', // Para onde a requisição será enviada
            data: `competencia=${competencia}`, // Os dados que estamos enviando. Aqui posso passar outros parametros ex: competencia=2018-10&x=10&y=1
            // Como o AJAX retorna um HTML puro, eu posso trocar definindo abaixo um JSON.
            dataType: 'json',

            // Caso tenha sucesso
            // Aqui eu posso usar a conotação ARROW FUNCTION. Ex:
            // success: dados => {}
            success: function (dados) {
                $("#numeroVendas").html(dados.numeroVendas)
                $("#totalVendas").html(dados.totalVendas)
                $("#clientesAtivos").html(dados.clientesAtivos)
                $("#clientesInativos").html(dados.clientesInativos)
                $("#totalDespesa").html(dados.totalDespesa)
                $("#totalReclamacao").html(dados.totalReclamacao)
                $("#totalElogios").html(dados.totalElogios)
                $("#totalSugestao").html(dados.totalSugestao)
            },

            // Caso tenha erro
            // Aqui eu posso usar a conotação ARROW FUNCTION. Ex:
            // error: erro => {}
            error: function (erro) {
                console.log(erro)
            }
        })
    })
})