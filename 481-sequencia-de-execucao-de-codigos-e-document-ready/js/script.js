// [EM JAVASCRIPT]
// O script seja implementado antes do elemento ser criado, ela retorna com erro.
// Para resolver isso, existe duas formas
// - 1º: <body onload="funcao()">
// - 2ª: window.onload = function(){...}

// [EM JQUERY]
// 1ª: $(document).ready(function(){...})
// 2ª- $(function(){...})


$(document).ready(function () {
    function teste() {
        console.log($('#exemplo1'))
    }
    teste()
})

function teste2() {
    console.log($('#exemplo2'))
}
$(teste2)