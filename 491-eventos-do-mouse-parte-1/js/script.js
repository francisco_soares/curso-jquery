// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {
    // [mousedown] - O evento acontece quando o mouse é pressionado
    $('#btn1').mousedown(() => {
        $("#div2").css('background-color', 'green')
    })
    // [mouseup] - O evento acontece no momento da liberação do clique do mouse
    $('#btn1').mouseup(() => {
        $('#div2').css('background-color', 'blue')
    })
    // [click] - Funciona como o [mouseup], o mesmo é a mistura do [mousedown] com [mouseup]
    $('#btn1').click(() => {
        $('#div2').css('background-color', 'blue')
    })
    // [dbclick] - Afeta o elemento quando você da um clique duplo.
    $('#btn2').dblclick(() => {
        $('#div2').css('background-color', '#ff8800')
    })
    // [mousemove] - O movimento do mouse sobre um elemento HTML. Ele é a base do efeitos drag and drop
    // o [e] referencia o próprio elemento, nesse caso a [#div2]
    $('#div2').mousemove((e) => {
        console.log(e.offsetX, e.offsetY)
        $('#resultadoDiv').html(`Posição X: ${e.offsetX} / Posição Y: ${e.offsetY}`)
    })
    // [mouseenter]
    // [mouseleave]
    // [mouseover]
    // [mouseout]

})