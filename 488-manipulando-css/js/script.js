// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {
    // css() - Adiciona propriedades de estilo inline
    // $('#topo').css('background-color', '#155661').css('margin', '0px').css('padding', '10px 20px 10px 20px')
    $('#topo').css({
        'background-color': '#155661',
        'margin': '0px',
        'padding': '10px 20px 10px 20px'
    })

    $('body').css('margin', '0')
    $('h1').css({
        'color': '#fff',
        'font-size': '24px'
    })

    // addClass() - Adiciona Classes
    $('input').addClass('campo padrao')
    $('textarea').addClass('campo erro')

    // hasClass() - Verifica a existência de uma classe no elemento HTML. Retorna TRUE ou FALSE
    console.log($('textarea').hasClass('padrao'))
    if ($('textarea').hasClass('padrao') == false) {
        console.log('Não possui a classe PADRAO')
    }
    // removeClass() - Remove classes
    if ($('textarea').hasClass('erro')) {
        $('textarea').removeClass('erro')
        $('textarea').addClass('padrao')
    }

})