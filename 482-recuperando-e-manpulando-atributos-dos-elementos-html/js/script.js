// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {
    // [attr(<atributo>, <valor>)] - Caso o valor seja omitido, o mesmo pega o valor do ATRIBUTO
    // O código abaixo vai retornar o caminho e nome da imagem, Ex: images/esfera_1.png
    console.log($('img').attr('src'))
    // Trás a borda definida na tag [IMG]. Resultado = 5
    console.log($('img').attr('border'))
    // Altero a imagem na tag [IMG]
    console.log($('img').attr('src', 'images/esfera_2.png'))
    // Altero a largura da borda definida na tag [IMG]
    console.log($('img').attr('border', 20))

    // Formato a [DIV] pelo método [style] do CSS.
    console.log($('div').attr('style', 'background-color: #ffcc00; width: 200px; height: 200px;'))

    // Recuperando o valor do [INPUT]
    console.log($('input').attr('value'))
    // Modificando o valor pelo [ATTR] do [jQuery]
    console.log($('input').attr('value', 'Modificado o valor pelo jQuery'))
    console.log($('input').attr('value'))

    // Alterando o [TYPE] do [FORM] para [PASSWORD]
    console.log($('input').attr('type', 'password'))
})