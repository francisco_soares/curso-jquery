// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// SEM  [ARROW FUNCTION]
$(document).ready(function () {
    // Animações - Atua sobre qualquer propriedade CSS numérica
    // Ou seja, propriedade que temos que definir valor, como font-size, margin, padding e etc.

    // animate(<propriedades visuais>, <opções da transição>)
    // $('button').on('click', function(){
    //     $('div').animate({
    //         'width': '160px',
    //         'height': '160px',
    //         'margin-left': '200px',
    //         'margin-top': '200px',
    //         'border-radius': '100%'
    //     }, 2000)
    // })

    $('button').on('click', function () {
        let visualFinal = {
            'width': '160px',
            'height': '160px',
            'margin-left': '200px',
            'margin-top': '200px',
            'border-radius': '100%'
        }
        $('#div1').animate(visualFinal, {
            duration: 3500,
            start: () => {
                console.log('Animação iniciada')
            },
            complete: () => {
                $('#div2').animate(visualFinal, 1500)
            }
        })
    })
})