// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {
    // trigger() - Aciona evento de modo programático
    $('#btn2').on('click', () => {
        alert('Botão 2 clicado!')
    })

    // A lógica aqui é, ao clicar no BTN 1, será acionado o click no BTN 2 pelo método trigger()
    $('#btn1').on('click', () => {
        $('#btn2').trigger('click')
    })

    // Se eu colocar o código assim e apenas atualizar a página, será apresentado o ALERT
    // $('#btn1').trigger('click')

    // hover() - Função auxiliar para captura dos eventos mouseenter/mouseout/mouseleave/mouseover
    // o [e] referencia a #DIV1
    /*$('#div1').on('mouseenter', e => {
        // addClass - Adiciona a classe emDestaque na #div1
        $(e.target).addClass('emDestaque')
    })*/

    /*$('#div1').on('mouseleave', e => {
        // removeClass - Adiciona a classe emDestaque na #div1
        $(e.target).removeClass('emDestaque')
    })*/

    // hover() - Espera duas função, porém você pode passar apenas uma.
    // Se passar apenas uma função, ela será usada para mouseenter/mouseout/mouseleave/mouseover
    $('#div1').hover(
        // A primeira função é usada para o [mouseenter ou mouseover]
        // Lembrando que estou usando ARROW FUNCTION
        e => $(e.target).addClass('emDestaque'),

        // A primeira função é usada para o [mouseout ou mouseleave]
        // Lembrando que estou usando ARROW FUNCTION
        e => $(e.target).removeClass('emDestaque')
    )

    // toggleClass() - Função auxiliar para inclusão/remoção de classe
    // Lembrando que estou usando ARROW FUNCTION
    // $('#div2').hover(e => $(e.target).toggleClass('emDestaque'))

    // Forma normal, sem ARROW FUNCTION
    $('#div2').hover(function(e){
        $(e.target).toggleClass('emDestaque')
    })
})