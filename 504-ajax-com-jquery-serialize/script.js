// Também posso usar a conotação ARROW FUNCTION:
//$(document).ready(() => {})
$(document).ready(function () {
    // Também posso usar a conotação ARROW FUNCION
    // $('button').on('click', function(){})
    $('button').on('click', function (e) {
        e.preventDefault()

        // serialize() - Pega todos os dados enviado do formulário e converte para um x-www-form-urlencoded.
        let dados = $('form').serialize()
        

        // Ajax
        $.ajax({
            // As ordens do atributos não implica no funcionamento
            type: 'get', // Tipo do método de envio
            url: 'app.php', // Para onde está sendo enviado
            data: dados, // O dados capturado do formulário
            dataType: 'json', // Informando o tipo de retorno, que deve ser JSON 
            success: dados => {console.log(dados)}, //Caso sucesso
            error: erro => {console.log(erro)} // Caso erro.
        })
    })
})

// [IMPORTANTE] - Nos campos [checkbox], o name deve ter os [colchetes], então, fica dessa forma, ex: name="interesses[]"