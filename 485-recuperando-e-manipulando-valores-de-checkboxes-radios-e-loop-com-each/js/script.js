// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {

    // Acessando valores do campo [RADIO] do [FORMULÁRIO]
    // NOTA: Execute o comando [$('.sexo:checked').val()] dentro do console do navegador para verificar o retorno quando clicado no check do radios.
    let sexo = $('.sexo:checked').val()
    console.log(sexo)

    // NOTA: Execute o comando [$('.interesse:checked')] dentro do console do navegador para verificar o retorno quando clicado no check do radios.
    // AVISO: Como o campo checkbox permite o selecionar mais de um elemento, caso você use o [.val()], ele vai retornar apenas o valor do 1º elemento selecionado. Para solucionar esse problema, vamos ter que usar o loop [each]
    let interesse = $('.interesse:checked')
    console.log(interesse)

    // LOOP [EACH] - É uma função nativa do jQuery, onde espera dois parametros (array, callback), então ficaria assim: $.each(array, callback)
    // No segundo parametro o [callback], será inserido uma função na forma que irei inseri será em [ARROW FUNCTION], recebe dois parametros (indice, valor)
    // NOTA: Para a função abaixo funcionar, você precisa inserir no console do navegador
    $.each($('.interesse:checked'), (indice, valor) => {
        console.log(indice, valor.value)
    })
})