// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// SEM  [ARROW FUNCTION]
$(document).ready(function () {

    // hide() - Esconde um elemento HTML
    // Você pode passar os parametros dentro do hide(), exemplo: hide(slow), hide(medio), hide(fast) ou hide(500) milisegundos
    // $('#div1').hide(5000)

    // show() - Exibe um elemento HTML
    // Você pode passar os parametros dentro do show(), exemplo: show(slow), show(medio), show(fast) ou show(500) milisegundos
    // $("#div1").show(5000);

    // toggle() - Exibe e esconde um elemento HTML (show/hide)
    // Você pode passar os parametros dentro do toggle(), exemplo: toggle(slow), toggle(medio), toggle(fast) ou toggle(500) milisegundos
    // $("#div1").toggle(5000)
    $("#btn1").on('click', () => {
        $('#div1').toggle('slow')
    })

    // fadeOut() - Faz com que o elemento desaparece com uma opacidade
    // Você pode passar os parametros dentro do fadeOut(), exemplo: fadeOut(slow), fadeOut(medio), fadeOut(fast) ou fadeOut(500) milisegundos
    $('#div2').fadeOut('slow')

    // fadeIn() - Faz com que o elemento apareca em forma de opacidade
    // Você pode passar os parametros dentro do fadeIn(), exemplo: fadeIn(slow), fadeIn(medio), fadeIn(fast) ou fadeIn(500) milisegundos
    $('#div2').fadeIn('slow')

    // fadeToggle() - Faz com que o elemento aparece e desaparece em opacidade (fadeOut/fadeIn) depende do estado do elemento
    // $('#div2').fadeToggle('slow')
    $('#btn2').on('click', function () {
        $('#div2').fadeToggle('slow')
    })

    // fadeTo() - Leva o elemento até um ponto especifico de opacidade. 0 = Invisivel, 1 = Visivel
    // Recebe 2 parametros: 1º - Se é slow, medio, fast ou milisegundos(500) | 2º - Até que ponto chega a opacidade 0 a 1
    // $('#div2').fadeTo('slow', 0.2)
    $('#btn2').on('click', function () {
        $('#div2').fadeTo('slow', 0.2)
    })

    // slideUp() - Desliza o elemento para cima 
    // Você pode passar os parametros dentro do slideUp(), exemplo: slideUp(slow), slideUp(medio), slideUp(fast) ou slideUp(500) milisegundos
    $('#div3').slideUp('slow')

    // slideDown() - Desliza o elemento para baixo
    // Você pode passar os parametros dentro do slideDown(), exemplo: slideDown(slow), slideDown(medio), slideDown(fast) ou slideDown(500) milisegundos
    $('#div3').slideDown('fast')

    // slideToggle() - Faz o slideUp() ou slideDown() dependendo do estado do elemento
    // $('#div3').slideToggle('slow')
    $("#btn3").on('click', function () {
        $('#div3').slideToggle('slide')
    })
})