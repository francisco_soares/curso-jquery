// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {

    // [onload]
    $("#onload").html('Página carregada')

    // [scroll]
    // [Window] - Guarda o estado da [JANELA DO NAVEGADOR]
    $(window).scroll(() => {
        //Assim que o Scroll do browser for acionado, será apresentado o texto abaixo.
        $('#scroll').html('Scroll acionado!')
    })

    // Usando [FUNCTION]
    /*$('#div1').scroll(function(){
        // O [this] está referenciando a [#div1] declarada anteriormente.
        $(this).css('background-color', 'blue')
    })*/

    // Usando [ARROW FUNCTION] - é bem diferente da FUNCTION
    $('#div1').scroll(e => {
        // O [this] está referenciando a [#div1] declarada anteriormente.
        $(e.target).css('background-color', 'blue')
    })

    // [resize]
    $(window).resize(() => {
        // Assim que a largura ou altura forem alterados, será retornado a função abaixo.
        $('#resize').html('Página redimencionada').css('background-color', '#d12121')
    })
})