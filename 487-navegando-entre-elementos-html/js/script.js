// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {

    // [parent()] - Elemento pai
    console.log('[parent()] - Elemento pai')
    console.log($('.secao1').parent())

    // [closest] - Procura por elementos pais
    console.log('[closest] - Procura por elementos pais')
    console.log($('.subSecao').closest('#pagina'))
    console.log($('.subSecao').closest(".secao1"))
    console.log($('.item').closest("#topo"))
    console.log($('.item').closest('ul'))

    // [find()] - Procura por elemento filhos
    console.log('[find()] - Procura por elemento filhos')
    console.log($('#rodape').find('p'))
    console.log($('#topo').find('.item'))

    console.log($('.secao1').parent().find('h1'))    
    console.log($('.subSecao').parent().parent().find('h1'))
    console.log($('.subSecao').closest('#pagina').find('h1'))

})