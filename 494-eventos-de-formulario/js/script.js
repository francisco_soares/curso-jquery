// $(document).ready(function(){...}) - Só executa os scripts quando o HTML estiver carregado.
// Usando [ARROW FUNCTION]
$(document).ready(() => {
    // focus() - Acionado com tem o foco(é clicado) no elemento html
    $('#nome').focus((e) => {
        console.log('Esse elemento HTML recebeu foco (NOME)')
        $(e.target).addClass('inputFocado')

        // O [e.target] traz a string InputEvent, exemplo: <input type="text" id="nome" class="inputFocado"></input>
        // Dessa forma no código acima é inserido a classe: $(e.target).addClass('inputFocado')
        console.log(e.target)
    })

    $('#email').focus((e) => {
        console.log('Esse elemento HTML recebeu foco (EMAIL)')
        $(e.target).addClass('inputFocado')
    })

    // blur() - Acionado com perde o foco no elemento html
    $('#nome').blur((e) => {
        console.log('Esse elemento HTML perdeu foco (NOME)')
        $(e.target).removeClass('inputFocado')
    })

    $('#email').blur((e) => {
        console.log('Esse elemento HTML perdeu foco (EMAIL)')
        $(e.target).removeClass('inputFocado')
    })

    // change() - Acionado quando um elemento html muda o valor (Select, checkbox ou radio)
    $('#opcao').change((e) => {
        let txt = $(e.target).val()
        console.log(`Opção foi modificada para: ${txt}`)
    })

    // submit() - Acionado quando o formulário é enviado.
    $('#form').submit((e) => {
        // preventDefault() - Previne o comportamento padrão do formulário, que seria enviar e atualizar a página.
        // Assim podemos controlar por aqui o que devemos fazer.
        // ATENÇÃO: Sempre tem que ser usada com o parametro passado, nesse caso o [e]
        e.preventDefault()
        console.log('Formulário enviado')
    })

    $('a').click((e) => {
        e.preventDefault()
        console.log('Ação do link parada')
    })
})